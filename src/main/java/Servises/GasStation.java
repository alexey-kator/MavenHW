package Servises;


import Vehicle.MyVehicle;

public class GasStation {
    private double tankCapacity;
    private double currentVolume;
    private double price;
    private MyVehicle myVehicle;

    public MyVehicle getMyVehicle() {
        return myVehicle;
    }

    public void setMyVehicle(MyVehicle myVehicle) {
        this.myVehicle = myVehicle;
    }

    public double getTankCapacity() {
        return tankCapacity;
    }

    public double getCurrentVolume() {
        return currentVolume;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void fillUp(){
        currentVolume = myVehicle.getFuelTank().getCurrentVolume();
        tankCapacity = myVehicle.getFuelTank().getTANK_CAPASITY();
        System.out.println("GasStation: Full fuel tank costs " + (tankCapacity-currentVolume)*price);
        myVehicle.getFuelTank().setCurrentVolume(tankCapacity);
    }
}

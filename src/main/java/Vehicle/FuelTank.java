package Vehicle;

public class FuelTank {

    private final double TANK_CAPASITY = 42.4;
    private double currentVolume;


    public double getCurrentVolume() {
        return currentVolume;
    }

    public void setCurrentVolume(double currentVolume) {
        this.currentVolume = currentVolume;
    }


    public double getTANK_CAPASITY() {
        return TANK_CAPASITY;
    }
}

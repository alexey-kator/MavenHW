package Vehicle;

public class Engine implements Workable{
    private int workingTime;
    private double currentFuelConsumption;
    Wheels wheels;

    public int getWorkingTime() {
        return workingTime;
    }

    public void setWorkingTime(int workingTime) {
        this.workingTime = workingTime;
    }

    public double getCurrentFuelConsumption() {
        return currentFuelConsumption;
    }

    public void setCurrentFuelConsumption(double currentFuelConsumption) {
        this.currentFuelConsumption = currentFuelConsumption;
    }

    public void setWheels(Wheels wheels) {
        this.wheels = wheels;
    }

    public void work() {
        System.out.println("Engine is working for "+ workingTime + " sec");
        wheels.setWorkingTime(workingTime);
        wheels.work();


    }
}

package Vehicle;

public class Wheels implements Workable {
    private int workingTime;

    public int getWorkingTime() {
        return workingTime;
    }
    public void setWorkingTime(int workingTime) {
        this.workingTime = workingTime;
    }

    public void work() {
        System.out.println("Wheels are spinning for "+ workingTime + " sec");
    }
}

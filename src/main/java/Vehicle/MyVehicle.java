package Vehicle;

public class MyVehicle implements Workable{
    private OnboardComputer onboardComputer;
    private Engine engine;
    private FuelTank fuelTank;
    private Wheels wheels;
    private int workingTime;

    public MyVehicle() {
        onboardComputer = new OnboardComputer();
        engine = new Engine();
        fuelTank = new FuelTank();
        wheels = new Wheels();
    }

    public OnboardComputer getOnboardComputer() {
        return onboardComputer;
    }

    public void setOnboardComputer(OnboardComputer onboardComputer) {
        this.onboardComputer = onboardComputer;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public FuelTank getFuelTank() {
        return fuelTank;
    }

    public void setFuelTank(FuelTank fuelTank) {
        this.fuelTank = fuelTank;
    }

    public Wheels getWheels() {
        return wheels;
    }

    public void setWheels(Wheels wheels) {
        this.wheels = wheels;
    }

    public int getWorkingTime() {
        return workingTime;
    }

    public void setWorkingTime(int workingTime) {
        this.workingTime = workingTime;
    }

    public void work() {
        onboardComputer.setEngine(engine);
        onboardComputer.setFuelTank(fuelTank);
        onboardComputer.setWorkingTime(workingTime);
        engine.setWheels(wheels);
        onboardComputer.work();

    }
}

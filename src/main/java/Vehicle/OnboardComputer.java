package Vehicle;

public class OnboardComputer implements Workable{
    private final double varningLevel = 10.0;
    private int workingTime;
    private boolean varning = false;
    private Engine engine;
    private FuelTank fuelTank;
    private double currentVolume;


    public double getVarningLevel() {
        return varningLevel;
    }

    public int getWorkingTime() {
        return workingTime;
    }

    public void setWorkingTime(int workingTime) {
        this.workingTime = workingTime;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public FuelTank getFuelTank() {
        return fuelTank;
    }

    public void setFuelTank(FuelTank fuelTank) {
        this.fuelTank = fuelTank;
    }

    public double getCurrentVolume() {
        return currentVolume;
    }

    public void setCurrentVolume(double currentVolume) {
        this.currentVolume = currentVolume;
    }

    public void work() {
        currentVolume = (fuelTank.getCurrentVolume() - (workingTime*engine.getCurrentFuelConsumption()));

        if ((fuelTank.getCurrentVolume() - (workingTime*engine.getCurrentFuelConsumption()))<=0){
            workingTime = (int)(fuelTank.getCurrentVolume()/engine.getCurrentFuelConsumption());
            currentVolume = 0;
        }
        engine.setWorkingTime(workingTime);
        engine.work();

        fuelTank.setCurrentVolume(currentVolume);
        if(currentVolume<varningLevel){
            if (currentVolume==0){
                System.out.println("Fuel tank is empty! ");
            } else {
                System.out.println("Please refuel. In fuel tank remain only " + currentVolume);
            }
        }
    }

}

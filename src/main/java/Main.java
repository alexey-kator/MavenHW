import Servises.GasStation;
import Vehicle.MyVehicle;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("root.xml");

        MyVehicle superCar = (MyVehicle)context.getBean("vehicle");

        GasStation gasStation = (GasStation) context.getBean("gas");


        superCar.work();
        System.out.println();
        superCar.getEngine().setCurrentFuelConsumption(2.1);
        superCar.setWorkingTime(15);
        superCar.work();

        gasStation.fillUp();
        System.out.println();

        superCar.getEngine().setCurrentFuelConsumption(2.1);
        superCar.setWorkingTime(20);
        superCar.work();


//        GasStation gasStation = new GasStation();
//        gasStation.setPrice(11.5);
//
//
//
//
//        MyVehicle superCar = new MyVehicle();
//        gasStation.setMyVehicle(superCar);
//
//        superCar.getFuelTank().setCurrentVolume(31.5);
//        superCar.getEngine().setCurrentFuelConsumption(1.1);
//        superCar.setWorkingTime(13);
//        superCar.work();
//        System.out.println();
//        superCar.getEngine().setCurrentFuelConsumption(2.1);
//        superCar.setWorkingTime(15);
//        superCar.work();
//
//        gasStation.fillUp();
//        System.out.println();
//
//        superCar.getEngine().setCurrentFuelConsumption(2.1);
//        superCar.setWorkingTime(20);
//        superCar.work();
    }
}
